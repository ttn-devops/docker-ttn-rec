#!/bin/sh
if [ ${REALTIME:-false} == true ]; then
  REALTIME_OPT=-R
else
  REALTIME_OPT=-r
fi

echo "jackd $REALTIME_OPT -d net -a${JACK_SERVER:-'127.0.0.1'} -n${CLIENT_NAME:-'aras-daemon'} 1> /dev/null 2> /dev/null &"
exec jackd $REALTIME_OPT -d net -a${JACK_SERVER:-'127.0.0.1'} -n${CLIENT_NAME:-'ttn-rec'} &
sleep ${JACK_TIMEOUT:-5}
exec jack-plumbing -q -u 100000 /opt/ttn-rec/jack.plumbing &
exec npm start
