FROM registry.gitlab.com/ttn-devops/docker-jackd

ENV RECORDINGS_DIR=/recordings/

RUN apt-get install -y git curl ffmpeg

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs

RUN mkdir -p /opt/ttn-rec/ && \
    mkdir /recordings && \
    cd /opt/ttn-rec && \
    git clone https://gitlab.com/ttanttakun/ttn-rec.git . && \
    npm install

COPY docker-entry-point.sh /usr/local/bin
EXPOSE 3000
VOLUME /recordings
WORKDIR /opt/ttn-rec
ENTRYPOINT ["/usr/local/bin/docker-entry-point.sh"]
