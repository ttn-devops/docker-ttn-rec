# docker-ttn-rec

## env vars

|var|what|
|---|---|
|JACK_SERVER| master jack server ip|
|CLIENT_NAME| (this) jack slave server name |
|IRRATSAIOAK_API|irratsaioak json API endpoint|
|FLOWR_API|post flowr hook endpoint|
|REC_SERVER|this server's url (http://)|
|PORT|http port (default: 3000)|

## EXPOSE
EXPOSE 3000

## VOLUME
VOLUME /recordings
